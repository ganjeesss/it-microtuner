#!/usr/bin/tclsh

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice
# is preserved.  This file is offered as-is, without any warranty.

proc write-string {fh string width} {
    puts -nonewline $fh [binary format "a${width}" $string]
}

proc write-char {fh char} {
    puts -nonewline $fh [binary format c $char]
}

proc write-short {fh short} {
    puts -nonewline $fh [binary format s $short]
}

proc write-int {fh int} {
    puts -nonewline $fh [binary format i $int]
}

proc blank {fh n} {
    puts -nonewline $fh [binary format x$n]
}

set infilename [lindex $argv 0]
set outfilename [lindex $argv 1]

exec sox $infilename -t u16 -r 44100 -c 1 $infilename.u16

set outfile [open $outfilename w]
fconfigure $outfile -translation binary

puts -nonewline $outfile "IMPS"

write-string $outfile $infilename 12
write-char $outfile 0
write-char $outfile 64
write-char $outfile 3
write-char $outfile 64
write-string $outfile $infilename 26
write-char $outfile 0
write-char $outfile 0
write-int $outfile [expr {int([file size $infilename.u16] / 2)}]
write-int $outfile 0
write-int $outfile 0
write-int $outfile 44100
write-int $outfile 0
write-int $outfile 0
write-int $outfile [expr {[tell $outfile] + 4}]
blank $outfile 4

set data [open $infilename.u16 r]
fconfigure $data -translation binary
fcopy $data $outfile

close $data
close $outfile

file delete $infilename.u16