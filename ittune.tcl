#!/usr/bin/tclsh

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice
# is preserved.  This file is offered as-is, without any warranty.

proc write-char {fh char} {
    puts -nonewline $fh [binary format c $char]
}

proc write-short {fh short} {
    puts -nonewline $fh [binary format s $short]
}

proc write-int {fh int} {
    puts -nonewline $fh [binary format i $int]
}

proc blank {fh n} {
    puts -nonewline $fh [binary format x$n]
}

proc write-sample-map {fh notes} {
    set map [list]
    set width [expr {(int(([llength $notes] - 1)/12)+1) * 12}]
    for {set i 1} {$i <= [llength $notes]} {incr i} {
        lappend map $i
    }
    while {[llength $map] < $width} {
        lappend map 1
    }
    for {set i 0} {$i < 120} {incr i} {
        set note [expr { int($i / $width) * 12 + (12 * (int($width / 12) - 1)) }]
        set sample [lindex $map [expr {$i % $width}]]
        write-char $fh $note
        write-char $fh $sample
    }
}

proc write-envelope-header {fh} {
    write-char $fh 0
    write-char $fh 2
    blank $fh 4
}

proc write-volume-envelope {fh} {
    write-envelope-header $fh
    write-char $fh 32
    write-short $fh 0
    write-char $fh 32 
    write-short $fh 32
    blank $fh 70
}

proc write-other-envelope {fh} {
    write-envelope-header $fh
    write-char $fh 0
    write-short $fh 0
    write-char $fh 0
    write-short $fh 16 
    blank $fh 70
}

proc write-header {fh notes} {
    puts -nonewline $fh "IMPI"
    blank $fh 20
    write-char $fh 128
    write-char $fh 256
    write-char $fh 0
    write-char $fh 0
    write-short $fh 0214
    write-char $fh [llength $notes]
    blank $fh 33
    write-sample-map $fh $notes
    write-volume-envelope $fh
    write-other-envelope $fh
    write-other-envelope $fh
    blank $fh 4
}

proc write-sample-header {fh infile cents poslist} {
    set outfile "$infile.$cents"
    file delete $outfile
    file copy $infile $outfile

    set samplefh [open $outfile r+]
    fconfigure $samplefh -translation binary
    seek $samplefh 0x3c
    binary scan [read $samplefh 4] i speed

    set frequency [expr {int($speed * (2 ** ($cents / 1200.0)))}]

    seek $samplefh 0x3c
    write-int $samplefh $frequency

    upvar $poslist p
    lappend p $outfile
    lappend p [expr [tell $fh] + 0x48]

    seek $samplefh 0
    fcopy $samplefh $fh -size 0x50

    close $samplefh
}

proc write-sample-body {fh infile pos} {
    seek $fh 0 end
    set position [tell $fh]

    set samplefh [open $infile r]
    fconfigure $samplefh -translation binary
    seek $samplefh 0x50
    fcopy $samplefh $fh
    close $samplefh

    seek $fh $pos
    write-int $fh $position

    file delete $infile
}

proc get-notes {} {
    set notes [list]

    while {[gets stdin note] > 0} {
        lappend notes [scan $note %f]
    }

    return $notes
}

set samplename [lindex $argv 0]
set instrumentname [lindex $argv 1]

set outfile [open $instrumentname w]
fconfigure $outfile -translation binary

set notes [get-notes]
write-header $outfile $notes

set poslist [list]
foreach note $notes {
    write-sample-header $outfile $samplename $note poslist
}

for {set i 0} {$i < [llength $poslist]} {incr i 2} {
    write-sample-body $outfile [lindex $poslist $i] [lindex $poslist [expr $i + 1]]
}
    
close $outfile