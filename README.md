# it microtuner

create microtonal impulse tracker instruments

## ittune.tcl

takes an ITS sample and a tuning and creates an ITI

    ./ittune.tcl sample.its instrument.iti < tuning.txt

where tuning.txt is a list of cent offsets for notes in the tuning (see quartertone.txt)

## wav2its.tcl

    ./wav2its.tcl sample.wav sample.its
    
takes a WAV sample and converts it to ITS